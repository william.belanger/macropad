#!/usr/bin/python3
import serial
import shlex
import subprocess
import time
from pathlib import Path


DEVICE, BAUDRATE = "/dev/macropad", "9600"
processes = {
    "a": "~/scripts/desktop/switch-desktop-vm.py",  # toggle vm-win / desktop-1
    "b": "wmctrl -r :ACTIVE: -b toggle,maximized_vert,maximized_horz",  # toggle maximize
    "c": "amixer set Master toggle",  # mute
    "d": "~/scripts/qoob/qoob/__init__.py --quiet --previous",  # previous
    "e": "~/scripts/qoob/qoob/__init__.py --play-pause",  # play/pause
    "f": "~/scripts/qoob/qoob/__init__.py --quiet --next",  # next
    "g": "~/scripts/desktop/docker/docker.py w 33% 100%",  # dock left
    "h": "~/scripts/desktop/docker/docker.py c 33% 100%",  # dock center
    "i": "~/scripts/desktop/docker/docker.py e 33% 100%",  # dock right
    "-1": "~/scripts/pulse-volume -5%",  # inc volume
    "1": "~/scripts/pulse-volume +5%",  # dec volume
}


def UART() -> serial.serialposix.Serial:
    try:
        uart = serial.Serial(DEVICE, BAUDRATE)
        uart.setDTR(False)  # Send a soft reset
        time.sleep(0.022)
        uart.setDTR(True)
        return uart
    except serial.serialutil.SerialException:
        time.sleep(2)


uart = UART()
while True:
    try:
        key = str(uart.readline(), "utf-8")
        key = key.rstrip()
        cmd = processes.get(key)
        cmd = Path(cmd).expanduser()
        cmd = shlex.split(str(cmd))
        if cmd:
            subprocess.Popen(cmd)
        uart.flushInput()
    except (AttributeError, serial.serialutil.SerialException):
        uart = UART()
