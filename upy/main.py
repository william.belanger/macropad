import time
import uasyncio as asyncio
from machine import Pin, UART
from rotary_irq_rp2 import RotaryIRQ
from ucollections import namedtuple

RE = namedtuple("RotaryEncoder", ("clock", "data", "switch"))
MATRIX = (
    ("a", "b", ""),
    ("d", "e", "f"),
    ("g", "h", "i")
)

ROWS = (3, 4, 5)
COLUMNS = (6, 7, 8)
ENCODER = RE(19, 20, 21)
uart = UART(1, 9600, bits=8, parity=None, stop=1)


def echo(text: str):
    uart.write(text)
    print(text)


class RotaryEncoder:
    def __init__(self, rotary_encoder, callback):
        self.cb = callback
        self.re = rotary_encoder
        self.re.add_listener(self._callback)
        self.re.reset()
        self.event = asyncio.Event()
        asyncio.create_task(self._activate())

    async def _activate(self):
        while True:
            await self.event.wait()
            value = self.re.value()
            if value:
                self.cb(value)
                self.re.reset()
            self.event.clear()

    def _callback(self):
        self.event.set()


class Callbacks:
    @classmethod
    def re_switch(cls, pin):
        if cls._debounce(pin):
            echo("c")

    @staticmethod
    def re_rotate(value):
        echo(str(value))

    def _debounce(pin):
        prev = None
        for _ in range(64):
            current_value = pin.value()
            if prev is not None and prev != current_value:
                return None
            prev = current_value
        return not prev


class Keys:
    def __init__(self):
        self.pressed = set()
        self.rows = [Pin(r, Pin.IN, Pin.PULL_DOWN) for r in ROWS]
        self.cols = [Pin(c, Pin.OUT) for c in COLUMNS]
        for c in self.cols:
            c.low()

    def scan(self):
        for c, col in enumerate(self.cols):
            col.high()
            for r, row in enumerate(self.rows):
                key = MATRIX[r][c]
                if row.value() and key not in self.pressed:
                    self.pressed.add(key)
                    echo(key)
                elif not row.value():
                    self.pressed.discard(key)
            col.low()
        time.sleep_ms(10)


async def main():
    re_switch = Pin(ENCODER.switch, Pin.IN, Pin.PULL_UP)
    re_switch.irq(trigger=Pin.IRQ_FALLING, handler=Callbacks.re_switch)
    RotaryEncoder(RotaryIRQ(pin_num_clk=ENCODER.clock,
                            pin_num_dt=ENCODER.data,
                            min_val=-1,
                            max_val=1,
                            range_mode=RotaryIRQ.RANGE_BOUNDED),
                  Callbacks.re_rotate)
    keys = Keys()

    while True:
        await asyncio.sleep_ms(10)
        keys.scan()


try:
    asyncio.run(main())
except (KeyboardInterrupt, Exception) as e:
    print('Exception {} {}\n'.format(type(e).__name__, e))
finally:
    ret = asyncio.new_event_loop()  # Clear retained uasyncio state
